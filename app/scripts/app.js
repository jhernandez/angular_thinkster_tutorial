'use strict';

/**
 * @ngdoc overview
 * @name angularProjectsApp
 * @description
 * # angularProjectsApp
 *
 * Main module of the application.
 */
var app = angular.module('angularProjectsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase'
]);
app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            redirectTo: '/posts'
        })
        .when('/about', {
            templateUrl: 'views/about.html',
            controller: 'AboutCtrl'
        })
        .when('/posts', {
            templateUrl: 'views/posts.html',
            controller: 'PostsCtrl'
        })
        .when('/posts/:postId', {
            templateUrl: 'views/showpost.html',
            controller: 'PostViewCtrl'
        })
        .when('/register', {
            templateUrl: 'views/register.html',
            controller: 'AuthCtrl'
        })
        .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'AuthCtrl'
        })
        .when('/users/:username', {
            templateUrl: 'views/profile.html',
            controller: 'ProfileCtrl'
        })
        .otherwise({
            redirectTo: '/posts'
        });
});
app.constant('FIREBASE_URL', 'https://vivid-fire-814.firebaseio.com/');